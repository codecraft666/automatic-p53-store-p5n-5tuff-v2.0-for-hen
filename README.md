# Automatic P53 store P5N 5tuff V2.0 for HEN

This include P5N games, DLCs, P51, P52 games, every with RAP

﻿Please, you can only use this store on portable USB mode, don't try install 
this store in your PS3, the libraries are too large for RAM memory
and it could damage the file system.

USER MANUAL:



1. You need: 
	
	a. PS3 with HFW and HEN.
 
	b. USB FAT 32 format.

 	c. PSN stuff in your computer (OPTIONAL)
 	
	d. A stable internet connection in your computer.
		
		*It is necesary for some C00 games and DLCs, 3 or 5 games.
		
2. On your PS3:
	
	a. The 14351 raps (Tools/Raps/14351...) and installed on your PS3.

	b. The package UNLOCK_c00 (Tools/C00unlocked/UNLOCK_c00.pkg) installed on your PS3.
	
	c. Some store with option for USB portable store -I recommend TheWizWiki store-.

3. Download the last database of PsnStuff from PsnStuff application (Update Database).

4. In PsnStuff's folder show the hidden files, open database.gz and decompress database file.

	*This file is not same than database on PusnStuff's folder.

5. Verify the date of modification of database file, if it is new regarding Tools/database... move it to Atomatic... folder.

	*If the database file is old respect to Tools/database..., copy and page the database of Tools/database... to Atomatic... folder.


6. Run AUTOMATIC_PSN_STUFF_x64.exe. In Automatic... folder you must to obtain:
7. 
	a. game folder with the store.

	b. NewRaps folder with raps don't be 14351 raps.
	
	c. NewEdats folder with unlock packages don't be UNLOCK_c00 package. 
	
7. Install, on your PS3, every NewRaps/exdata in hd00/exdata by means of some Homebrew (I recommend the Irisman or Multiman).

8. Install, on your PS3, every NewEdats/* by means of packages install.

9. Move the game folder to USB on FAT 32 format.

10. Congratulations, now you can download games from: XMB of your store->USB portable stores->Freeshop (of Cyb3r)->Games...
	
    *I recommend first turn on your ps3, connect your USB in dev_usb000 of PS3, download the games, disconnect USB before turn off PS3. It can avoid to damage the file system of your PS3.
